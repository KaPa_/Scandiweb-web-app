
<div class="topnav">
    <p class="split">Product Add</p>
    <form action="/index">
        <button type="submit" class="btn btn-outline-secondary" id="cancel">Cancel</button>
    </form>
    <button type="submit" form="product_form" class="btn btn-outline-primary" id="submit">Save</button>
</div>
<?php if (!empty($errors)) {?>
    <div class="alert alert-danger">
        <?php foreach ($errors as $error) { ?>
            <div>
                <?php echo $error ?>
            </div>
        <?php }?>
    </div>
<?php }?>

<div class="formDiv">

    <form method="post" id="product_form" >
        <div class="form-group row">
            <label for="sku" class="col-sm-1 col-form-label">SKU</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="sku" name="sku" placeholder="#SKU" value="<?php echo $product["sku"]  ?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-sm-1 col-form-label">Name</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="name" name="name" placeholder="#Name" value="<?php echo $product["name"] ?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="price" class="col-sm-1 col-form-label">Price ($)</label>
            <div class="col-sm-3">
                <input type="number" step="0.01" min="0.00" max="9999.99" class="form-control" id="price" name="price" placeholder="#Price" value="<?php echo $product["price"] ?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="productType" class="col-sm-2 col-form-label">Product Type</label>
            <div class="col-sm-2">
                <select id="productType" name="productType" class="form-control">
                    <option value="">Select Product Type</option>
                    <option value="DVD">DVD</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                </select>
            </div>
        </div>

        <div id="productAttributes">
        </div>
        <div id="notification" class="notification"></div>
    </form>
</div>


<script src="../js/script.js"></script>
