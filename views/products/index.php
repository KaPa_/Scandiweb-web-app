<div class="topnav">

    <p class="split">Product List</p>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <button type="submit" name="add" class="btn btn-md btn-outline-success" id="ADD">Add</button>
    </form>

    <form method="post" action="/index" >
        <button type="submit" name="submit" class="btn btn-md btn-outline-danger" id="massDelete" >Mass Delete</button>
</div>
<div class="container">
    <div class="box-container">

        <?php foreach ( $products as $product) { ?>
            <div class="box">
                <div class="form-check">
                    <input class="form-check-input checkbox-position delete-checkbox" type="checkbox" name="delete[]" value="<?php echo $product['id']; ?>" id="checkbox-<?php echo $product['id']; ?>"> </div>
                <p><?php echo $product["sku"]?></p>
                <p><?php echo $product["name"]?></p>
                <p><?php echo $product["price"]." $"?></p>
                <p>
                    <?php
                    if ($product["size"]) {
                        echo "Size: ".$product["size"]." MB";
                    }elseif ($product["weight"]){
                        echo "Weight: ".$product["weight"]." KG";
                    }else{
                        echo "Dimension: ".$product["height"]."x".$product["width"]."x".$product["length"];
                    }
                    ?>
                </p>
            </div>
        <?php }?>

    </div>
    </form>
</div>
