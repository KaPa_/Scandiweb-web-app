<?php

namespace app\models;
use app\Database;

require_once "Product.php";
class Book extends Product
{
    protected $weight;


    public function setWeight($weight)
    {
        if ($this->type === "Book" && ($weight === null || $weight === "" || $weight < 0)) {
            throw new \InvalidArgumentException("Please enter correct weight of book");
        }
        $this->weight = $weight;
    }

    public function getWeight()
    {
        if ($this->weight === null || $this->weight === "" || $this->weight < 0) {
            throw new \InvalidArgumentException("Book weight is not set or invalid");
        }
        return $this->weight;
    }

    public function load($data)
    {

        try {
            $this->setSku($data["sku"]);
            $this->setName($data["name"]);
            $this->setPrice($data["price"]);
            $this->setType($data["type"]);
            $this->setWeight($data["weight"]);
        }catch (\InvalidArgumentException $e)
        {
            return $e->getMessage();
        }
    }

    public function saveProduct()
    {
        $db = Database::$db;
        $db->createProduct($this);
    }

}