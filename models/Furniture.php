<?php

namespace app\models;
use app\Database;
require_once "Product.php";
class Furniture extends Product
{
    protected $width;
    protected $height;
    protected $length;

    public function setWidth($width)
    {
        if ($this->type === "Furniture" && ($width === null || $width === "" || $width < 0)) {
            throw new \InvalidArgumentException("Please enter correct width ");
        }
        $this->width = $width;
    }

    public function getWidth()
    {
        if ($this->width === null || $this->width === "" || $this->width < 0) {
            throw new \InvalidArgumentException("Furniture width is not set or invalid");
        }
        return $this->width;
    }

    public function setHeight($height)
    {
        if ($this->type === "Furniture" && ($height === null || $height === "" || $height < 0)) {
            throw new \InvalidArgumentException("Please enter correct height");
        }
        $this->height = $height;
    }

    public function getHeight()
    {
        if ($this->height === null || $this->height === "" || $this->height < 0) {
            throw new \InvalidArgumentException("Furniture height is not set or invalid");
        }
        return $this->height;
    }

    public function setLength($length)
    {
        if ($this->type === "Furniture" && ($length === null || $length === "" || $length < 0)) {
            throw new \InvalidArgumentException("Please enter correct length");
        }
        $this->length = $length;
    }

    public function getLength()
    {
        if ($this->length === null || $this->length === "" || $this->length < 0) {
            throw new \InvalidArgumentException("Furniture length is not set or invalid");
        }
        return $this->length;
    }

    public function load($data)
    {
        try {
            $this->setSku($data["sku"]);
            $this->setName($data["name"]);
            $this->setPrice($data["price"]);
            $this->setType($data["type"]);
            $this->setWidth($data["width"]);
            $this->setHeight($data["height"]);
            $this->setLength($data["length"]);
        }catch (\InvalidArgumentException $e)
        {
            return $e->getMessage();
        }
    }

    public function saveProduct()
    {
        $db = Database::$db;
        $db->createProduct($this);
    }
}