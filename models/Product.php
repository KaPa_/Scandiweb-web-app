<?php

namespace app\models;


abstract class Product
{
    protected $sku;

    protected $name;
    protected $price;

    protected $type;


    public function setSku($sku) {
        if ($sku === null || $sku === "") {
            throw new \InvalidArgumentException("Please enter product SKU");
        }
        $this->sku = $sku;
    }

    public function getSku() {
        if ($this->sku === null || $this->sku === "") {
            throw new \InvalidArgumentException("Product SKU is not set");
        }
        return $this->sku;
    }

    public function setName($name) {
        if ($name === null || $name === "") {
            throw new \InvalidArgumentException("Please enter product name");
        }
        $this->name = $name;
    }

    public function getName() {
        if ($this->name === null || $this->name === "") {
            throw new \InvalidArgumentException("Product name is not set");
        }
        return $this->name;
    }

    public function setPrice($price) {
        if ($price === null || $price === "" || $price < 0) {
            throw new \InvalidArgumentException("Please enter product pryce");
        }
        $this->price = $price;
    }

    public function getPrice() {
        if ($this->price === null || $this->price === "" || $this->price < 0) {
            throw new \InvalidArgumentException("Product price is not set or invalid");
        }
        return $this->price;
    }

    public function setType($type) {
        if (!($type === "Book" || $type === "DVD" || $type === "Furniture")) {
            throw new \InvalidArgumentException("Please choose a Product Type");
        }
        $this->type = $type;
    }

    public function getType() {
        if (!($this->type === "Book" || $this->type === "DVD" || $this->type === "Furniture")) {
            throw new \InvalidArgumentException("Product type is not set or invalid");
        }
        return $this->type;
    }

    abstract public function load($data);
    abstract public function saveProduct();

}