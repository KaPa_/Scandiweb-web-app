<?php

namespace app\models;
use app\Database;
require_once "Product.php";
class Dvd extends Product
{
    protected $size;

    public function setSize($size)
    {
        if ($this->type === "Dvd" && ($size === null || $size === "" || $size < 0)) {
            throw new \InvalidArgumentException("Please enter correct size of DVD");
        }
        $this->size = $size;
    }

    public function getSize()
    {
        if ($this->size === null || $this->size === "" || $this->size < 0) {
            throw new \InvalidArgumentException("DVD size is not set or invalid");
        }
        return $this->size;
    }

    public function load($data)
    {
        try {
            $this->setSku($data["sku"]);
            $this->setName($data["name"]);
            $this->setPrice($data["price"]);
            $this->setType($data["type"]);
            $this->setSize($data["size"]);
        }catch (\InvalidArgumentException $e){
            return $e->getMessage();
        }
    }

    public function saveProduct()
    {
        $db = Database::$db;
        $db->createProduct($this);
    }
}