<?php

namespace app\controllers;

use app\models\Book;
use app\models\Dvd;
use app\models\Furniture;
use app\Router;

class ProductController
{
    public static function index(Router $router)
    {
        $products = $router->database->getProducts();
        $router->renderView("products/index", [
            "products" => $products
        ]);
    }


    public static function create(Router $router)
    {

        $productData = self::sanitizeProductData($_POST);
        $errors = [];

        $product = $router->database->getProductBySKU($productData["sku"]);

        if (!empty($product)){
            $errors[] = "this SKU already exists please choose another one ";
        }

        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            $productType = $productData["type"] ?? '';


            if (!empty($productType)) {
                $product = self::instantiateProduct($productType);
                $errors[] = $product->load($productData);

                if (empty($errors[0])) {
                    $product->saveProduct();
                }
            }
            else {
                $errors[] = "Please choose a valid product type.";
            }
        }

        $router->renderView("products/create", ["errors" => $errors],$productData);
    }

    public static function delete(Router $router)
    {
        $idsToDelete = $_POST['delete'] ?? null;


        if (!$idsToDelete) {
            header("Location: /products");
            exit;
        }

        if ($router->database->deleteProducts($idsToDelete)) {
            header("Location: /index");
            exit;
        }
    }

    private static function sanitizeProductData($postData)
    {
        return [
            "sku" => $postData["sku"] ?? '',
            "name" => $postData["name"] ?? '',
            "price" => $postData["price"] ?? '',
            "type" => $postData["productType"] ?? '',
            "weight" => $postData["weight"] ?? null,
            "size" => $postData["size"] ?? null,
            "height" => $postData["height"] ?? null,
            "width" => $postData["width"] ?? null,
            "length" => $postData["length"] ?? null,
        ];
    }

    private static function instantiateProduct($productType)
    {
        switch ($productType) {
            case 'Book':
                return new Book();
            case 'DVD':
                return new Dvd();
            case 'Furniture':
                return new Furniture();
        }
    }
}
