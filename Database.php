<?php

namespace app;

use PDO;
use app\models\Dvd;
use app\models\Book;
use app\models\Product;
use app\models\Furniture;
class Database
{

    private $dsn = 'mysql:host=127.0.0.1; port=3306; dbname=web_app_project';
    private $username = "root";
    private $password = "";
    public $pdo = null;
    public static ?Database $db = null;
    public function __construct()
    {
        $this->pdo = new PDO($this->dsn, $this->username, $this->password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$db = $this;
    }

    public function getProducts()
    {
        $statement = $this->pdo->prepare("SELECT * FROM products ORDER BY id DESC ");

        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProductBySKU($sku)
    {
        $statement = $this->pdo->prepare("SELECT * FROM products WHERE sku = :sku");

        $statement->bindValue(":sku", $sku);

        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function deleteProducts($idsToDelete)
    {
        $statement = $this->pdo->prepare("DELETE FROM products WHERE id IN (" . implode(',', array_fill(0, count($idsToDelete), '?')) . ")");

        $statement->execute($idsToDelete);

        header("Location: index");
        exit();
    }

    public function createProduct(Product $product)
    {
        $statement = null;

        if ($product instanceof Book) {
            $statement = $this->pdo->prepare("INSERT INTO products (sku, name, price, type, weight)
                                          VALUES (:sku, :name, :price, :type, :weight)");
            $statement->bindValue(":weight", $product->getWeight());
        } elseif ($product instanceof Dvd) {
            $statement = $this->pdo->prepare("INSERT INTO products (sku, name, price, type, size)
                                          VALUES (:sku, :name, :price, :type, :size)");
            $statement->bindValue(":size", $product->getSize());
        } elseif ($product instanceof Furniture) {
            $statement = $this->pdo->prepare("INSERT INTO products (sku, name, price, type, length, width, height)
                                          VALUES (:sku, :name, :price, :type, :length, :width, :height)");
            $statement->bindValue(":length", $product->getLength());
            $statement->bindValue(":width", $product->getWidth());
            $statement->bindValue(":height", $product->getHeight());
        }

        if ($statement) {
            $statement->bindValue(":sku", $product->getSku());
            $statement->bindValue(":name", $product->getName());
            $statement->bindValue(":price", $product->getPrice());
            $statement->bindValue(":type", $product->getType());
            $statement->execute();

        }



        header("Location: index.php");
    }
}