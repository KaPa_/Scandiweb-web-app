<?php

use app\controllers\ProductController;
use app\Router;
use app\Database;

require_once __DIR__."/../vendor/autoload.php";

$database = new Database();
$router = new Router($database);



$router->get("/", [ProductController::class, "index"]);
$router->get("/index", [ProductController::class, "index"]);
$router->get("/create", [ProductController::class, "create"]);
$router->post("/create", [ProductController::class, "create"]);
$router->post("/index", [ProductController::class, "delete"]);

$router->resolve();

?>