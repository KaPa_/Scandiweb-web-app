
document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('productType').value = '';
});

document.getElementById('productType').addEventListener('change', function () {
    var type = this.value;
    var attributesDiv = document.getElementById('productAttributes');
    var notificationDiv = document.getElementById('notification');
    attributesDiv.innerHTML = '';

    if (type === 'DVD') {
        attributesDiv.innerHTML = `
            <div class="form-group row">
                <label for="size" class="col-sm-2 col-form-label">Size (MB)</label>
                <div class="col-sm-2">
                    <input type="number" step="0.01" min="0.00" max="9999.99" class="form-control" id="size" name="size" placeholder="#Size">
                </div>
            </div>
        `;
        notificationDiv.textContent = 'Please provide size (MB)';
    } else if (type === 'Book') {
        attributesDiv.innerHTML = `
            <div class="form-group row">
                <label for="weight" class="col-sm-2 col-form-label">Weight (Kg)</label>
                <div class="col-sm-2">
                    <input type="number" step="0.01" min="0.00" max="9999.99" class="form-control" id="weight" name="weight" placeholder="#Weight">
                </div>
            </div>
        `;
        notificationDiv.textContent = 'Please provide weight (Kg)';
    } else if (type === 'Furniture') {
        attributesDiv.innerHTML = `
            <div class="form-group row">
                <label for="height" class="col-sm-2 col-form-label">Height</label>
                <div class="col-sm-2">
                    <input type="number" step="0.01" min="0.00" max="9999.99" class="form-control" id="height" name="height" placeholder="#Height">
                </div>
            </div>
            <div class="form-group row">
                <label for="width" class="col-sm-2 col-form-label">Width</label>
                <div class="col-sm-2">
                    <input type="number" step="0.01" min="0.00" max="9999.99" class="form-control" id="width" name="width" placeholder="#Width">
                </div>
            </div>
            <div class="form-group row">
                <label for="length" class="col-sm-2 col-form-label">Length</label>
                <div class="col-sm-2">
                    <input type="number" step="0.01" min="0.00" max="9999.99" class="form-control" id="length" name="length" placeholder="#Length">
                </div>
            </div>
        `;
        notificationDiv.textContent = 'Please provide dimensions';
    }
});

document.getElementById('product_form').addEventListener('submit', function (event) {

});
